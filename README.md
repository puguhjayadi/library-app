<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

## 1. Clone the Git Repository
- Open your terminal or command prompt.
- Navigate to your desired project directory.
- Use the git clone command to clone the repository.

`git clone <repository_url> <folder_name>`

## 2. Install Composer Dependencies
- Laravel uses Composer for PHP dependency management.
- Navigate to your project folder.
- Run composer install to install PHP dependencies.

`composer install`

## 3. Generate an application key.
`php artisan key:generate`

## 4. Setup .env
- Duplicate the `.env.example` file and rename it to `.env`
- Open the `.env` file and set your database connection details.

```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=your_database_name
DB_USERNAME=your_database_username
DB_PASSWORD=your_database_password
```

## 5. Migrate dan Seeder the Database
Refresh the database and run all database seeds

`php artisan migrate:fresh --seed`

## 6. Start the Development Server
Use Artisan or XAMPP to start the Laravel development server.

`php artisan serve`
