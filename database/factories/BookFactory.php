<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class BookFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => "Book ".$this->faker->unique()->numberBetween($min = 1, $max = 50),
            'author' => $this->faker->name(),
            'year' => $this->faker->randomElement([2000, 2001, 2002]),
        ];
    }
}
