<?php

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;
use App\Models\Book;

class BookUnitTest extends TestCase
{

    // public function test_index_method()
    // {
    //         // Membuat beberapa buku untuk diindeks
    // $books = Book::factory()->count(5)->create();

    // // Membuat instance BookController
    // $controller = new BookController();

    // // Memanggil method index
    // $result = $controller->index();

    // // Memastikan bahwa hasilnya adalah instance dari Illuminate\Http\Response
    // $this->assertInstanceOf(Response::class, $result);

    // // Memastikan bahwa data buku yang dikembalikan sesuai dengan yang diharapkan
    // foreach ($books as $book) {
    //     $this->assertJson($result->getContent()); // Memastikan bahwa response berformat JSON
    //     $this->assertStringContainsString($book->title, $result->getContent()); // Memastikan bahwa judul buku ada dalam response
    // }
    // }

    //  public function test_store_method()
    // {
    //     // Data buku yang akan disimpan
    //     $bookData = [
    //         'title' => 'New Book',
    //         'author' => 'Author Name',
    //         'year' => 2022
    //     ];

    //     // Mengirimkan permintaan POST ke endpoint /books
    //     $response = $this->post('/books', $bookData);

    //     // Memastikan bahwa respons memiliki status 302 (Redirect)
    //     $response->assertStatus(302);

    //     // Memastikan bahwa buku berhasil disimpan di database
    //     $this->assertDatabaseHas('books', $bookData);
    // }

    // public function test_edit_method()
    // {
    //     // Membuat buku untuk diubah
    //     $book = Book::factory()->create();

    //     // Mengirimkan permintaan GET ke endpoint /books/{id}/edit
    //     $response = $this->get("/books/{$book->id}/edit");

    //     // Memastikan bahwa respons memiliki status 200
    //     $response->assertStatus(200);

    //     // Memastikan bahwa data buku ditampilkan di halaman edit
    //     $response->assertSee($book->title);
    // }

    // public function test_update_method()
    // {
    //     // Membuat buku untuk diubah
    //     $book = Book::factory()->create();

    //     // Data baru untuk buku
    //     $newData = [
    //         'title' => 'Updated Title',
    //         'author' => 'Updated Author',
    //         'year' => 2023
    //     ];

    //     // Mengirimkan permintaan PUT ke endpoint /books/{id}
    //     $response = $this->put("/books/{$book->id}", $newData);

    //     // Memastikan bahwa respons memiliki status 302 (Redirect)
    //     $response->assertStatus(302);

    //     // Memastikan bahwa buku telah diperbarui di database
    //     $this->assertDatabaseHas('books', $newData);
    // }

    // public function test_destroy_method()
    // {
    //     // Membuat buku untuk dihapus
    //     $book = Book::factory()->create();

    //     // Mengirimkan permintaan DELETE ke endpoint /books/{id}
    //     $response = $this->delete("/books/{$book->id}");

    //     // Memastikan bahwa respons memiliki status 302 (Redirect)
    //     $response->assertStatus(302);

    //     // Memastikan bahwa buku telah dihapus dari database
    //     $this->assertDeleted($book);
    // }
}
